//
//  RequestHelper.swift
//  LoL Fantasy
//
//  Created by jmoralez on 6/23/14.
//  Copyright (c) 2014 Bathroom Gaming LLC. All rights reserved.
//

import Foundation

class RequestHelper : NSObject {
    class func printCookie(cookie : NSHTTPCookie) {
        println("*********************COOOKIE********************")
        println("Comment: \(cookie.comment)")
        println("CommentURL: \(cookie.commentURL)")
        println("Domain: \(cookie.domain)")
        println("Expires Date: \(cookie.expiresDate)")
        println("Is HTTP Only: \(cookie.HTTPOnly)")
        println("Is Secure: \(cookie.secure)")
        println("Is Session Only: \(cookie.sessionOnly)")
        println("Name: \(cookie.name)")
        println("Path: \(cookie.path)")
        println("Port List: \(cookie.portList)")
        println("Properties: \(cookie.properties)")
        println("Value: \(cookie.value)")
        println("Version: \(cookie.version)")
        println("*****************END COOOKIE********************")
    }
    
    class func getAuthenticationCookieString() -> String {
        var cookieString  = "PVPNET_REGION="   + NSUserDefaults.standardUserDefaults().stringForKey("PVPNET_REGION")   + "; "
            cookieString += "PVPNET_LANG="     + NSUserDefaults.standardUserDefaults().stringForKey("PVPNET_LANG")     + "; "
            cookieString += "PVPNET_ACCT_NA="  + NSUserDefaults.standardUserDefaults().stringForKey("PVPNET_ACCT_NA")  + "; "
            cookieString += "PVPNET_ID_NA="    + NSUserDefaults.standardUserDefaults().stringForKey("PVPNET_ID_NA")    + "; "
            cookieString += "PVPNET_TOKEN_NA=" + NSUserDefaults.standardUserDefaults().stringForKey("PVPNET_TOKEN_NA")
        
        return cookieString
    }
}