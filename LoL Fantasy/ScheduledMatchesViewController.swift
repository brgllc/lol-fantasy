//
//  ScheduledMatchesViewController.swift
//  LoL Fantasy
//
//  Created by jmoralez on 6/15/14.
//  Copyright (c) 2014 Bathroom Gaming LLC. All rights reserved.
//

import UIKit

class ScheduledMatchesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var matchesTableView : UITableView
    @IBOutlet var weekScrollView : UIScrollView
    
    let kCellReuseIdentifier: NSString = "ScheduledMatchCell"
    let kLeagueAPIURL: String = "http://fantasy.na.lolesports.com/en-US/api/season/4"
    let kLCSAPIURL = "http://na.lolesports.com/api/programming.json/?parameters[method]=all&parameters[week]=API_WEEK&parameters[tournament]=102&parameters[expand_matches]=1"
    
    var data      = NSMutableData()
    var tableData = NSArray()
    var teams     = NSDictionary()
    var matches   = NSDictionary()
    
    var currWeek  = 1
    
    var progressIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // CD examples
        let newTest = TestEntity.create() as TestEntity
        var results = TestEntity.findAll()
        println(results.count)
        // CD examples
        
        var buttonX: Float = 0.0
        let buttonWidth: Float = 100.0
        let buttonHeight: Float = 54.0
        for index in 1..<12 {
            let button = UIButton(frame: CGRectMake(buttonX, 0, buttonWidth, buttonHeight))
            button.tag = index
            button.addTarget(self, action: Selector("someButtonPressed:"), forControlEvents: UIControlEvents.TouchUpInside)
            
            var weekButtonTitleNormal: NSAttributedString = NSAttributedString(string: "Week\n\(index)",
                attributes: [NSForegroundColorAttributeName : UIColor(red: 196, green: 146, blue: 70, alpha: 1)])
            var weekButtonTitleSelected: NSAttributedString = NSAttributedString(string: "Week\n\(index)",
                attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
            button.setAttributedTitle(weekButtonTitleNormal, forState: UIControlState.Normal)
            button.setAttributedTitle(weekButtonTitleSelected, forState: UIControlState.Selected)
            
//            button.setTitle("\(index)", forState: UIControlState.Normal)
//            button.setTitleColor(UIColor(red: 196, green: 146, blue: 70, alpha: 1), forState: UIControlState.Normal)
//            button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Selected)
            
            button.titleLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
            button.titleLabel.textAlignment = NSTextAlignment.Center
            
            button.setBackgroundImage(UIImage(named: "week_btn_normal"), forState: UIControlState.Normal)
            button.setBackgroundImage(UIImage(named: "week_btn_selected"), forState: UIControlState.Selected)
            weekScrollView.subviews[0].addSubview(button) // just blindly selecting for now
            buttonX += buttonWidth
            
            if index == 1 {
                button.selected = true
            }
        }
        
        // For JMO
        // When I set the frame size of the subview (my container for the buttons) to 1100 in storyboard, you see that it looks correct.
        // But this should really be set programmatically (default value was 480), but when I do so, it won't scroll past ~week 5.
        // Not sure why. Now it's your turn. I'm thinking might be frame/contentsize of the scrollview itself, but I tried playing with that too with no luck

        println("before sub frame size: \(weekScrollView.subviews[0].frame)")
        let containerView = weekScrollView.subviews[0] as UIView
        containerView.frame = CGRect(x: 0, y: 0, width: 1100, height: containerView.frame.height)
        println("after sub frame size: \(weekScrollView.subviews[0].frame)")
        
//        getLeagueData()
        getMatchesForWeek(currWeek)

    }
    
    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
    }
    
    func someButtonPressed(button: UIButton) {
        println("button pressed: \(button.titleLabel.text)")
        var currButton = weekScrollView.subviews[0].viewWithTag(currWeek) as UIButton
        currButton.selected = false
        
        button.selected = true
        currWeek = button.tag
        
        getMatchesForWeek(currWeek)
    }
    
    /*
    *   Tableview Datasource Methods
    */
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        var matchInfoForDay = self.tableData[section] as NSDictionary
        var matches         = matchInfoForDay["matches"] as NSArray
        return matches.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return self.tableData.count
    }
    
    func tableView(tableView: UITableView!, titleForHeaderInSection section: Int) -> String! {
        return "  Week \(currWeek) Day \(section+1)"
    }
    
    func tableView(tableView: UITableView!, willDisplayHeaderView view: UIView!, forSection section: Int) {
        var header = view as UITableViewHeaderFooterView;
        header.textLabel.textColor = UIColor.whiteColor()
        header.contentView.backgroundColor = UIColor.grayColor()
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellReuseIdentifier, forIndexPath: indexPath) as UITableViewCell
        
//        var nameLabel = cell.viewWithTag(100) as UILabel
//        nameLabel.text = "Test"
        
        var matchInfoForDay = self.tableData[indexPath.section] as NSDictionary
        var matches          = matchInfoForDay["matches"] as NSArray
        var matchInformation = matches[indexPath.row] as NSDictionary
        
        var winnerId = matchInformation["winnerId"] as String

        var contestants = matchInformation["contestants"] as NSDictionary
        
        var redTeamInfo = contestants["red"] as NSDictionary
        var redTeamName = cell.viewWithTag(100) as UILabel
        redTeamName.text = redTeamInfo["name"] as String

        var blueTeamInfo = contestants["blue"] as NSDictionary
        var blueTeamName = cell.viewWithTag(200) as UILabel
        blueTeamName.text = blueTeamInfo["name"] as String
        
        var redTeamMatchResults = cell.viewWithTag(101) as UILabel
        var blueTeamMatchResults = cell.viewWithTag(201) as UILabel
        if (matchInformation["isFinished"] as String == "1") {
            if (winnerId == redTeamInfo["id"] as String) {
                redTeamMatchResults.text = "Win"
                blueTeamMatchResults.text = "Loss"
            } else {
                redTeamMatchResults.text = "Loss"
                blueTeamMatchResults.text = "Win"
            }
        } else {
            redTeamMatchResults.text = ""
            blueTeamMatchResults.text = ""
        }
        
        return cell
    }
    
    /*
    *   Network Calls
    */
    func getLeagueData() {
        var url: NSURL = NSURL(string: kLeagueAPIURL)
        var request: NSURLRequest = NSURLRequest(URL: url)
        var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)
        
        println("Retrieve League Data from URL: \(url)")
        
        connection.start()
    }
    
    func getMatchesForWeek(week: Int) {
        var requestString = kLCSAPIURL.stringByReplacingOccurrencesOfString("API_WEEK", withString: "\(week)", options: nil, range: nil)
        var url: NSURL = NSURL(string: requestString)
        var request: NSURLRequest = NSURLRequest(URL: url)
        var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)
        
        println("Retrieve League Data from URL: \(url)")
        
        progressIndicator.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)
        progressIndicator.center = self.view.center
        self.view.addSubview(progressIndicator)
        progressIndicator.bringSubviewToFront(self.view)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        progressIndicator.startAnimating()
        
        connection.start()
    }
    
    /*
    *  NSURLConnect Delegate
    */
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        self.data = NSMutableData()
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        var err: NSError
        var jsonResult: NSArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
        
        if jsonResult.count>0 {
            self.tableData = jsonResult
            
            self.matchesTableView.reloadData()
        }
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        progressIndicator.stopAnimating()
    }
    
    /*
     *
     */
    func getWeekMatches(week: Int) -> NSMutableArray {
        var weekMatches = NSMutableArray()
        println("Getting week \(week) matches")
        for (var i = 0; i < self.matches.allKeys.count; i++) {
            var matchId   = self.matches.allKeys[i] as String
            var match     = self.matches[matchId] as NSDictionary
            var matchWeek = match["week"] as Int
            if matchWeek == week {
                println("Match on \(matchId)")
                weekMatches.addObject(self.matches[matchId])
            }
        }
        
        return weekMatches
    }
}