//
//  TeamPlayersTableViewController.swift
//  LoL Fantasy
//
//  Created by jmoralez on 6/15/14.
//  Copyright (c) 2014 Bathroom Gaming LLC. All rights reserved.
//

import Foundation
import UIKit

class TeamPlayersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var tableData: NSDictionary = NSDictionary()
    
    /*
    *   Tableview Datasource Methods
    */
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        var numRows = tableData.count
        return numRows
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "TeamPlayerCell")
        
        var rowKey: NSString = self.tableData.allKeys[indexPath.row] as NSString
        var rowData: NSDictionary = self.tableData[rowKey] as NSDictionary
        var playerName: NSString = rowData["name"] as NSString
        var playPositions: NSArray = rowData["positions"] as NSArray
        var positionsString: NSMutableString = ""
        
        for (var i = 0; i < playPositions.count; i++) {
            positionsString.appendString(playPositions[i] as NSString)
            if (i+1 != playPositions.count) {
                positionsString.appendString(", ")
            }
        }
        
        cell.text = playerName
        cell.detailTextLabel.text = positionsString
        
        return cell
    }
}