//
//  ViewController.swift
//  LoL Fantasy
//
//  Created by jmoralez on 6/14/14.
//  Copyright (c) 2014 Bathroom Gaming LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate {
    
    @IBOutlet var appsTableView : UITableView
    
    let kLeagueAPIURL: String = "http://fantasy.na.lolesports.com/en-US/api/season/4"
    
    var data: NSMutableData = NSMutableData()
    var tableData: NSDictionary = NSDictionary()
    var players: NSDictionary = NSDictionary()
    
    /*
    *   View Lifecycle
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        println("Hello world")
        getLeagueData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        var destinationVC: TeamPlayersViewController = segue.destinationViewController as TeamPlayersViewController
        destinationVC.tableData = sender as NSDictionary
    }
    
    /*
     *   Tableview Datasource Methods
     */
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        var numRows = tableData.count
        return numRows
    }

    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "MyTestCell")
        var rowKey: NSString = self.tableData.allKeys[indexPath.row] as NSString
        var rowData: NSDictionary = self.tableData[rowKey] as NSDictionary
        var teamName: NSString = rowData["name"] as NSString
        var flavorTextArray: NSArray = rowData["flavorTextEntries"] as NSArray
        var flavorTextDict: NSDictionary = flavorTextArray[0] as NSDictionary
        var flavorText: NSString = flavorTextDict["flavorText"] as NSString
        
        cell.text = teamName
        cell.detailTextLabel.text = flavorText
        
        return cell
    }
    
    /*
     *  Tableview Delegate Methods
     */
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        // Load to other view with players on that team
        println("Selected Row \(indexPath.row)")
        
        var teamKey: NSString = self.tableData.allKeys[indexPath.row] as NSString
        var teamData: NSDictionary = self.tableData[teamKey] as NSDictionary
        var teamID: NSNumber = teamData["id"] as NSNumber
        
        println("Team ID: \(teamID)")
        
        var totalNumPlayers: Int = self.players.allKeys.count
        println("Total Number of Players: \(totalNumPlayers)")
        
        var teamPlayers: NSMutableDictionary = NSMutableDictionary()
        for (var i = 0; i < totalNumPlayers; i++) {
            var playerId: NSString = self.players.allKeys[i] as NSString
//            println("Current Player ID: \(playerId)")
            var player: NSDictionary = self.players[playerId] as NSDictionary
            if player["proTeamId"] as NSNumber == teamID {
                var playerName: NSString = player["name"] as NSString
                println("Match on \(playerName)")
                teamPlayers.setObject(player, forKey: playerId)
            }
        }
        
        self.performSegueWithIdentifier("selectedTeam", sender: teamPlayers)
    }
    
    /*
     *   Network Calls
     */
    func getLeagueData() {
        var url: NSURL = NSURL(string: kLeagueAPIURL)
        var request: NSURLRequest = NSURLRequest(URL: url)
        var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)
        
        println("Retrieve League Data from URL: \(url)")
        
        connection.start()
    }
    
    /* 
     *  NSURLConnect Delegate
     */
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        self.data = NSMutableData()
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        var err: NSError
        var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
        
        if jsonResult.count>0 && jsonResult["proTeams"].count>0 {
            var results: NSDictionary = jsonResult["proTeams"] as NSDictionary
            self.tableData = results
            self.appsTableView.reloadData()
            
            var players: NSDictionary = jsonResult["proPlayers"] as NSDictionary
            self.players = players
        }
    }
}

