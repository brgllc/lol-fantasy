//
//  SettingsTableViewController.swift
//  LoL Fantasy
//
//  Created by jmoralez on 6/23/14.
//  Copyright (c) 2014 Bathroom Gaming LLC. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    let kCellReuseIdentifier: NSString = "SettingsTableViewCell"
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController.navigationBarHidden = true
    }
    
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellReuseIdentifier, forIndexPath: indexPath) as UITableViewCell
        
        switch (indexPath.section, indexPath.row) {
            case (0,0):
                cell.textLabel.text = "Log In"
            default:
                println("Default")
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        switch (indexPath.section, indexPath.row) {
            case (0,0):
                var mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                self.navigationController.pushViewController(mainStoryboard.instantiateViewControllerWithIdentifier("LogInViewController") as UIViewController, animated: true)
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            default:
                println("Default")
        }
    }
}