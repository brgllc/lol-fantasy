//
//  LogInViewController.swift
//  LoL Fantasy
//
//  Created by jmoralez on 6/20/14.
//  Copyright (c) 2014 Bathroom Gaming LLC. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet var username : UITextField
    @IBOutlet var password : UITextField
    @IBOutlet var results  : UITextView
    @IBOutlet var logInButton : UIButton
    @IBOutlet var webView : UIWebView
    
    var data = NSMutableData()
    var progressIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController.navigationBarHidden = false
    }
    
    override func viewDidLoad() {
        webView.scalesPageToFit = true
        webView.dataDetectorTypes = .All
        
        let requestURL = NSURL(string: "http://fantasy.na.lolesports.com/en-US/")
        let request = NSURLRequest(URL: requestURL)
        webView.loadRequest(request)
    }
    
    /*
    *  NSURLConnect Delegate
    */
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        self.data = NSMutableData()
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        var err: NSError
//        var jsonResult: NSArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
        
        results.text = NSString(data:self.data, encoding:NSUTF8StringEncoding)
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        progressIndicator.stopAnimating()
    }
    
    /*
     *  UIWebViewDelegate Methods
     */
    func webViewDidStartLoad(_: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        var success = false
        
        var availableCookies = NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies
        for cookie : NSHTTPCookie in availableCookies as NSHTTPCookie[] {
//            RequestHelper.printCookie(cookie)
            var name  = cookie.name
            var value = cookie.value
            if (!value.isEmpty) {
                if (name == "PVPNET_REGION") {
                    NSUserDefaults.standardUserDefaults().setObject(value, forKey: "PVPNET_REGION")
                } else if (name == "PVPNET_LANG") {
                    NSUserDefaults.standardUserDefaults().setObject(value, forKey: "PVPNET_LANG")
                } else if (name == "PVPNET_ACCT_NA") {
                    NSUserDefaults.standardUserDefaults().setObject(value, forKey: "PVPNET_ACCT_NA")
                } else if (name == "PVPNET_ID_NA") {
                    NSUserDefaults.standardUserDefaults().setObject(value, forKey: "PVPNET_ID_NA")
                } else if (name == "PVPNET_TOKEN_NA") {
                    NSUserDefaults.standardUserDefaults().setObject(value, forKey: "PVPNET_TOKEN_NA")
                    success = true
                }
            }
        }
        
        if (success) {
            // remove the web view
            var leagueNumbers = NSMutableArray()
            var links = webView.stringByEvaluatingJavaScriptFromString("function getLinks() { var links = \"\";var leagues = document.getElementsByClassName('league');for (var i = 0; i < leagues.length; i++) {var item = leagues[i].getElementsByTagName('a')[0];links += item.href + \";\";}return links;}getLinks();")
            var linkUrls = links.componentsSeparatedByCharactersInSet(NSCharacterSet (charactersInString: ";"))
            for link : String in linkUrls {
                if (!link.isEmpty) {
                    var pieces = link.componentsSeparatedByCharactersInSet(NSCharacterSet (charactersInString: "/"))
                    if (pieces.count > 1) {
                        var leagueNumber = pieces[pieces.count - 1]
                        println("League Number: \(leagueNumber)")
                        leagueNumbers.addObject(leagueNumber)
                    }
                }
            }
            
            NSUserDefaults.standardUserDefaults().setObject(leagueNumbers, forKey: "LEAGUE_NUMBERS")
        }
    }

    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    
}