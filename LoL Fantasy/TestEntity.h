//
//  TestEntity.h
//  LoL Fantasy
//
//  Created by logen on 6/17/14.
//  Copyright (c) 2014 Bathroom Gaming LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TestEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * testInt;

@end
