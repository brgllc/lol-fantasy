//
//  MatchUpsViewController.swift
//  LoL Fantasy
//
//  Created by jmoralez on 6/22/14.
//  Copyright (c) 2014 Bathroom Gaming LLC. All rights reserved.
//

import UIKit

// http://fantasy.na.lolesports.com/en-US/api/league/267919

class MatchUpsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var results : UITextView
    @IBOutlet var fantasyMatchesTableView : UITableView
    
    let kFantasyCellReuseIdentifier: NSString = "FantasyMatchCell"
    let kLeagueAPIURL: String = "http://fantasy.na.lolesports.com/en-US/api/league/"
    
    var data      = NSMutableData()
    var tableData = NSArray()
    var progressIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
    
    var currWeek  = 1
    
    var fantasyTeams = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
        var leagueNumbers = NSUserDefaults.standardUserDefaults().arrayForKey("LEAGUE_NUMBERS") as String[]
        if (leagueNumbers.count > 0) {
            getLeagueData(leagueNumbers[0])
        } else {
            showNoLeaguesAlert()
        }
    }
    
    func showNoLeaguesAlert() {
        let title = NSLocalizedString("No Leagues Found", comment: "")
        let message = NSLocalizedString("Sorry, Unable to find any leagues. Please make sure you are logged in under the Settings tab.", comment: "")
        let cancelButtonTitle = NSLocalizedString("OK", comment: "")
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        // Create the action.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) { action in
            NSLog("The simple alert's cancel action occured.")
        }
        
        // Add the action.
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    /*
    *   Network Calls
    */
    func getLeagueData(leagueNumber: String) {
        var url: NSURL = NSURL(string: kLeagueAPIURL + leagueNumber)
        var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.setValue(RequestHelper.getAuthenticationCookieString(), forHTTPHeaderField: "Cookie")
        
        var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)
        
        println("Retrieve League Data from URL: \(url)")
        
        connection.start()
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return 5 //tableData.count
    }

    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        var cell = tableView.dequeueReusableCellWithIdentifier(kFantasyCellReuseIdentifier, forIndexPath: indexPath) as UITableViewCell!

//        var matchInformation = self.tableData[indexPath.row] as NSDictionary
//        
//        var redTeamInfo = matchInformation["redTeam"] as NSDictionary
//        var redTeamId = redTeamInfo["id"] as String
//        var redTeamName = cell!.viewWithTag(100) as UILabel
//        var redFantasyTeam = fantasyTeams[redTeamId] as NSDictionary
//        redTeamName.text =  redFantasyTeam["name"] as String
//        
//        var blueTeamInfo = matchInformation["blueTeam"] as NSDictionary
//        var blueTeamId = blueTeamInfo["id"] as String
//        var blueTeamName = cell!.viewWithTag(200) as UILabel
//        var blueFantasyTeam = fantasyTeams[blueTeamId] as NSDictionary
//        blueTeamName.text = blueFantasyTeam["name"] as String
//        
//        var redTeamMatchResults = cell!.viewWithTag(101) as UILabel
//        var blueTeamMatchResults = cell!.viewWithTag(201) as UILabel
        
        return cell!
    }
    
    /*
    *  NSURLConnect Delegate
    */
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        self.data = NSMutableData()
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data.appendData(data)
    }
    
    func connection(connection: NSURLConnection!, didFailWithError error: NSError!) {
        self.results.text = error.description
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        var err: NSError
        var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
        
        if jsonResult.count > 0 {
            // Get Fantasy Teams
            self.fantasyTeams = jsonResult["fantasyTeams"] as NSMutableDictionary
            if (self.fantasyTeams.count <= 0) {
                return;
            }
            
            var fantasyMatchesByWeek = jsonResult.objectForKey("fantasyMatchesByWeek") as NSDictionary
            if (fantasyMatchesByWeek.count <= 0) {
                return;
            }
            
            // Get Fantasy Matches
            var fantasyMatches = jsonResult.objectForKey("fantasyMatches") as NSDictionary
            if (fantasyMatches.count <= 0) {
                return;
            }
            
            var weekMatches = NSMutableArray()
            println("Getting week \(currWeek) matches")
//            for (var i = 0; i < fantasyMatches.allKeys.count; i++) {
//                var matchId   = fantasyMatches.allKeys[i] as String
//                var match     = fantasyMatches[matchId] as NSDictionary
//                var matchWeek = match["week"] as Int
//                if matchWeek == currWeek {
//                    println("Match on \(matchId)")
//                    weekMatches.addObject(match)
//                    if (weekMatches.count == fantasyTeams.count / 2) {
//                        println("Found all the matches for the week. Breaking out of search.")
//                        break;
//                    }
//                }
//            }
            
//            var weekMatchesKeys = fantasyMatchesByWeek.objectForKey(String(currWeek)) as NSArray
            var weekMatchesKeys = fantasyMatchesByWeek[String(currWeek)] as NSArray
            if (weekMatchesKeys.count <= 0) {
                return;
            }
            
//            for matchKey : String in weekMatchesKeys {
//                weekMatches.addObject(fantasyMatches.objectForKey(matchKey))
//            }
            
            for (var i = 0; i < weekMatchesKeys.count; i++) {
                var matchKey = String(weekMatchesKeys[i] as Int)
                println("Match key: \(matchKey)")
                weekMatches.addObject(fantasyMatches[matchKey])
            }

            self.tableData = weekMatches
            self.fantasyMatchesTableView.reloadData()
        }
        
//        var string = NSString(data: self.data, encoding: NSUTF8StringEncoding)
//        self.results.text = string
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        progressIndicator.stopAnimating()
    }
}